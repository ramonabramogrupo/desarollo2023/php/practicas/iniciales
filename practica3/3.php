<?php

// array inicializado
// es de tipo asociativo
$numeros = [
    "cero" => 0,
    "uno" => 1,
    "dos" => 2,
    "tres" => 3,
    "cuatro" => 4,
];

// analizar el contenido y el tipo del array
var_dump($numeros);

/** otra forma de inicializar **/
$numeros1 = [];

$numeros1["cero"] = 0;
$numeros1["uno"] = 1;
$numeros1["dos"] = 2;
$numeros1["tres"] = 3;
$numeros1["cuatro"] = 4;

// analizo el contenido del array
var_dump($numeros1);
