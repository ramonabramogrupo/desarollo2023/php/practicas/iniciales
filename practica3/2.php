<?php

/**
 * crear array
 */

/** opcion 1 **/
// array enumerado
$vocales = [
    'a',
    'e',
    'i',
    'o',
    'u'
];

/** opcion 2 **/
// array enumerado
$vocales1 = array('a', 'e', 'i', 'o', 'u');

/** opcion 3 **/
$vocales2 = [];
$vocales2[] = 'a';
$vocales2[] = 'e';
$vocales2[] = 'i';
$vocales2[] = 'o';
$vocales2[] = 'u';
// para realizar un push
// $vocales2[] = 'dato a introducir';
// array_push($vocales2, 'dato a introducir', 'otro dato');


/** opcion 4 **/
// array enumerado
$vocales3 = [
    0 => 'a',
    1 => 'e',
    2 => 'i',
    3 => 'o',
    4 => 'u'
];

var_dump($vocales);
var_dump($vocales1);
var_dump($vocales2);
var_dump($vocales3);
