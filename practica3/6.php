<?php

// array enumerado de vocales
$vocales = ['a', 'e', 'i', 'o', 'u'];

// mostrar la longitud del array con una funcion
// funcion count(array)

echo count($vocales); // 5
echo "<br>";

// mostrar una vocal aleatorio utilizando mt_rand
// funcion mt_rand
// mt_rand(minimo,maximo) ==> devuelve numero aleatorio entre min y max

$indice = mt_rand(0, 4); // devuelve numero aleatorio entre 0 y 4

echo $vocales[$indice]; // leo una vocal de forma aleatoria

// mostrar una vocal aleatorio utilizando mt_rand
$indice = mt_rand(0, 4);
echo $vocales[$indice];

// opcion 2
// puedo colocar la funcion dentro de la lectura
echo $vocales[mt_rand(0, 4)];
