<?php

// es un array enumerado de arrays asociativos
$datos = [
    [
        "nombre" => "Eva",
        "edad" => 50
    ],
    [
        "nombre" => "Jose",
        "edad" => 40,
        "peso" => 80
    ]
];

// mostrar el nombre del primer dato
// $datos[registro][campo]

echo $datos[0]["nombre"];
echo "<br>";

// mostrar el nombre del segundo dato
// $datos[registro][campo]

echo $datos[1]["nombre"];
echo "<br>";

// mostrar el peso
// $datos[registro][campo]

echo $datos[1]["peso"];
