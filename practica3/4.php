<?php

// array asociativo inicializado

$numeros = [
    "cero" => 0,
    "uno" => 1,
    "dos" => 2,
    "tres" => 3,
    "cuatro" => 4,
];

// array enumerado inicializado
$vocales = ['a', 'e', 'i', 'o', 'u'];


// leer el segundo elemento del array numeros y mostrarlo

echo $numeros["uno"];
echo "<br>";

// utilizar la notacion de las llaves
echo "{$numeros["uno"]}<br>";

// leer el segundo elemento del array vocales y mostrarlo
echo $vocales[1];
echo "<br>";

// utilizar la notacion de impresion corta
?>
<?= $vocales[1] ?><br>

<?php

// añadir un elemento al final de vocales con la a con tilde
// me piden realizar un push

/* opcion 1 */
// esta opcion me obliga a saber el primer hueco libre
// count($vocales) me daria la longitud del array
$vocales[5] = 'á';

/* opcion 2 */
// esta opcion es la recomendable cuando solo introduces un dato
$vocales[] = 'á';

/* opcion 3 */
// esta opcion es la recomendable cuando introduces mas de un dato
array_push($vocales, 'á');

// añadir un elemento nuevo al array numeros 
// con indice cinco con el valor 5

$numeros['cinco'] = 5;

var_dump($vocales, $numeros);
