<?php

$datos = [
    [
        "nombre" => "Eva",
        "edad" => 50
    ],
    [
        "nombre" => "Jose",
        "edad" => 40,
        "peso" => 80
    ]
];


// mostrar el numero de registros 
echo count($datos); // 2
echo "<br>";

// mostrar el numero de campos que tiene el registro eva

echo count($datos[0]); // 2
echo "<br>";
