<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $edad = 15; // variable de tipo entero
    ?>
    <p>
        <?= $edad ?>
    </p>
    <?php
    // cambio el valor de la variable
    $edad = 30;
    ?>
    <p>
        <?= $edad ?>
    </p>


</body>

</html>