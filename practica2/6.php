<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    // opcion 1
    // no es valida para las clases
    define("NOMBRE", "Ramon");

    // opcion 2
    // es valida siempre

    const NOMBRE1 = "Ramon";

    // imprimir las constantes
    echo NOMBRE;
    echo NOMBRE1;


    ?>
</body>

</html>