<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $radio = 2.4; // asignar el radio del circulo

    // calcular el perimetro
    $perimetro = 2 * M_PI * $radio;

    // calcular el area
    $area = pi() * $radio ** 2; // utilizando operador potencia

    $area = pi() * pow($radio, 2); // utilizando funcion potencia

    ?>

    <div>
        El radio del circulo es <?= $radio ?>
    </div>

    <div>
        El area del circulo es <?= $area ?>
    </div>
    <div>
        El perimetro del circulo es <?= $perimetro ?>
    </div>

    <div>
        <?php
        // mostrar el area con printf formateada
        printf("El area es %.2f", $area);
        ?>
    </div>


    <?php
    // quiero sacar las salidas sin tantos decimales
    $area = round($area, 3);
    $perimetro = round($perimetro, 1);
    ?>

    <div>
        El area del circulo es <?= $area ?>
    </div>
    <div>
        El perimetro del circulo es <?= $perimetro ?>
    </div>

</body>

</html>