<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 2</title>
</head>

<body>
    <h1>Ejercicio 2 de la practica 1</h1>
    <table width="100%" border="1">
        <tr>
            <td>
                <?php
                // Podemos utilizar o comillas simples o comillas doble para el texto
                echo "Este texto esta escrito utilizando la funcion echo de PHP";
                ?>
            </td>
            <td>Este texto esta escrito en HTML</td>
        </tr>
        <tr>
            <td>
                <?php
                print ' Este texto esta escrito desde PHP con la funcion print';
                ?>
            </td>
            <td>
                <?php
                // metodo largo
                echo "Centro de Formacion Alpe";
                // vamos a imprimir lo mismo con el metodo corto
                ?>
                <?= "Centro de Formacion Alpe" ?>
            </td>
        </tr>
    </table>

</body>

</html>